package co.lujun.androidbluetoothdemo;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "BlueToothTest";
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_ENABLE_BT_DISCOVERABLE = 2;
    private static final int MESSAGE_READ = 101;
    private static final int MESSAGE_WRITE = 102;

    private int mBluetoothState = RESULT_OK - 1;
    private int mLatestTime;
    private boolean mIsFinding = false, mServerStart = false;

    private Button btnFind, btnMakeDiscoverable, btnSend;
    private CheckBox cbBTStatus;
    private EditText tvDiscoverableTime, tvSend;
    private ListView lvPaired, lvFounded;
    private ProgressBar pbFinding, pbWaiting;
    private TextView tvSurpusTime, tvRec;

    private BluetoothAdapter mBluetoothAdapter;
    private BaseAdapter mPairedAdapter, mFoundAdapter;
    private List<String> mPairedList, mFoundList;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private String mDeviceName;

    private MyHandler mHandler;
    private ConnectThread mConnectThread;
    private AcceptThread mAcceptThread;
    private ConnectedThread mConnectedThread;
    private BlueToothReceiver mReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();
    }

    private void init(){
        btnFind = (Button) findViewById(R.id.btn_start_found);
        btnMakeDiscoverable = (Button) findViewById(R.id.btn_make_discoverable);
        btnSend = (Button) findViewById(R.id.btn_send);
        cbBTStatus = (CheckBox) findViewById(R.id.cb_tb_switch);
        tvDiscoverableTime = (EditText) findViewById(R.id.tv_discoverable_time);
        tvSend = (EditText) findViewById(R.id.et_send);
        lvPaired = (ListView) findViewById(R.id.lv_paired);
        lvFounded = (ListView) findViewById(R.id.lv_founded);
        tvSurpusTime = (TextView) findViewById(R.id.tv_surplus_time);
        tvRec = (TextView) findViewById(R.id.tv_rec);
        pbFinding = (ProgressBar) findViewById(R.id.pb_found);
        pbWaiting = (ProgressBar) findViewById(R.id.pb_waiting);

        btnFind.setOnClickListener(this);
        btnMakeDiscoverable.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        cbBTStatus.setOnCheckedChangeListener(this);

        mHandler = new MyHandler(this);

        // 注册接受BlueTooth发送的广播
        IntentFilter filter = new IntentFilter();
        // Bluetooth状态
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        // BluetoothDevice被发现
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        // discoverable mode
        filter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        // register
        mReceiver = new BlueToothReceiver();
        registerReceiver(mReceiver, filter);

        mPairedList = new ArrayList<String>();
        mFoundList = new ArrayList<String>();
        mPairedAdapter = new ArrayAdapter<String>(this, R.layout.text_item, mPairedList);
        mFoundAdapter = new ArrayAdapter<String>(this, R.layout.text_item, mFoundList);

        lvPaired.setAdapter(mPairedAdapter);
        lvFounded.setAdapter(mFoundAdapter);

        lvFounded.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemStr =  mFoundList.get(position);
                String address = itemStr.substring(itemStr.length() - 17);
                connect(address);
            }
        });
        lvPaired.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        onBlueToothTest();
    }

    private void onBlueToothTest(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null){
            // 设备不支持蓝牙，没有然后了...
            Toast.makeText(this, "不支持蓝牙", Toast.LENGTH_SHORT).show();
            cbBTStatus.setClickable(false);
            return;
        }

        // 检查蓝牙是否是enable
        if (!mBluetoothAdapter.isEnabled()){
            // 没有开启则提示开启
            Toast.makeText(this, "蓝牙未开启，请先开启蓝牙", Toast.LENGTH_SHORT).show();
            return;
        }else {
            mBluetoothState = RESULT_OK;
            cbBTStatus.post(new Runnable() {
                @Override
                public void run() {
                    cbBTStatus.setChecked(true);
                }
            });
            setBTSwitch(true);
        }

        if(mAcceptThread == null){
            mAcceptThread = new AcceptThread();
        }
        setAcceptThreadStatus(true);
        mAcceptThread.start();
    }

    public void setAcceptThreadStatus(boolean isStart){
        mServerStart = isStart;
    }

    private void checkPairedDevice(){
        mPairedList.clear();
        // 获取配对设备
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        // 有配对的设备
        if (pairedDevices.size() > 0){
            //
            for (BluetoothDevice device : pairedDevices) {
//                Log.d(TAG, "Paired----name:" + device.getName() + ", address:" + device.getAddress());
                mPairedList.add(device.getName() + "@" + device.getAddress());
            }
            notifyDataChanged();
        }
    }

    private void findDevice(){
        if (!mIsFinding) {
            mFoundList.clear();
            notifyDataChanged();
            // 查找设备
            mBluetoothAdapter.startDiscovery();
        }else {
            // connection之前必须使用下句代码关闭discovery
            mBluetoothAdapter.cancelDiscovery();
        }
        mIsFinding = !mIsFinding;
    }

    private void onBluetoothSwitch(boolean isStart){
        if (isStart){
            // 开启蓝牙
            Intent intent = new Intent();
            intent.setAction(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
            // 根据android.permission.BLUETOOTH_ADMIN直接开启权限
//            mBluetoothAdapter.enable();
        }else {
            findDevice();
            // 关闭蓝牙
            mBluetoothAdapter.disable();
            mBluetoothState = RESULT_OK - 1;
            setBTSwitch(false);
        }
    }

    private void notifyDataChanged(){
        mFoundAdapter.notifyDataSetChanged();
        mPairedAdapter.notifyDataSetChanged();
    }

    private void setBTSwitch(boolean isOpen){
        if (isOpen){
            cbBTStatus.setText("开启");
            checkPairedDevice();
        }else {
            cbBTStatus.setText("关闭");
        }
    }

    private void sendData(){
        if (TextUtils.isEmpty(tvSend.getText().toString()) || mConnectedThread == null){
            return;
        }
        mConnectedThread.write(tvSend.getText().toString().getBytes());
    }

    private void connect(String address){
        pbWaiting.setVisibility(View.VISIBLE);
        // 通过蓝牙地址获取远端设备handle
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (mConnectThread == null){
            mConnectThread = new ConnectThread(device);
        }
        mConnectThread.start();
    }

    private void manageConnectedSocket(BluetoothSocket socket) {
        setAcceptThreadStatus(false);
        if (mConnectedThread == null){
            mConnectedThread = new ConnectedThread(socket);
        }
        mConnectedThread.start();
    }

    private void manageConnectedSocket4Client(BluetoothSocket socket){
        if (mConnectedThread == null) {
            mConnectedThread = new ConnectedThread(socket);
        }
        mConnectedThread.start();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.cb_tb_switch){
            if (isChecked){
                onBluetoothSwitch(true);
            }else {
                onBluetoothSwitch(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_start_found){
            switch (mBluetoothState){
                case RESULT_OK - 1:
                    Toast.makeText(this, "蓝牙未开启，请先开启蓝牙", Toast.LENGTH_SHORT).show();
                    break;

                case RESULT_OK:
                    findDevice();
                    break;
            }
        }else if (id == R.id.btn_make_discoverable){
            String time = tvDiscoverableTime.getText().toString();
            if (TextUtils.isEmpty(time)){
                Toast.makeText(this, "请设置时间，单位s", Toast.LENGTH_SHORT).show();
                return;
            }
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, Integer.valueOf(time));
            startActivityForResult(intent, REQUEST_ENABLE_BT_DISCOVERABLE);
        }else if (id == R.id.btn_send){
            sendData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK){
            mBluetoothState = RESULT_OK;
            setBTSwitch(true);
        }else if (requestCode == REQUEST_ENABLE_BT_DISCOVERABLE){
            mLatestTime = resultCode;
            btnMakeDiscoverable.setClickable(false);
            mTimer = new Timer();
            mTimerTask = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvSurpusTime.setText("剩余：" + --mLatestTime + "s");
                            if (mLatestTime == 0){
                                if (mTimer != null){
                                    if (mTimerTask != null){
                                        mTimerTask.cancel();
                                    }
                                    mTimer.cancel();
                                }
                            }
                        }
                    });
                }
            };
            mTimer.schedule(mTimerTask, 0, 1000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private class BlueToothReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null){
                return;
            }
            String action = intent.getAction();
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
                int pre_state = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, 0);
                Log.d(TAG, "state: " + state + ", pre_state: " + pre_state);
            }else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
                Log.d(TAG, "found---start");
                btnFind.setText("停止");
                pbFinding.setVisibility(View.VISIBLE);
            }else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                Log.d(TAG, "found---finished");
                btnFind.setText("搜寻");
                pbFinding.setVisibility(View.INVISIBLE);
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.d(TAG, "found----name:" + device.getName() + ", address:" + device.getAddress());
                mFoundList.add(device.getName() + "@" + device.getAddress());
                notifyDataChanged();
            } else if (BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(action)){
                int scan_mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, 0);
                int pre_scan_mode = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE, 0);
                Log.d(TAG, "scan----scan_mode:" + scan_mode + ", pre_scan_mode:" + pre_scan_mode);
            }
        }
    }

    private void setData(String data) {
        tvRec.append(data + "\n");
    }

    private String getClientName(){
        return mDeviceName;
    }

    private static class MyHandler extends Handler{

        private static WeakReference<Activity> mWeakReference;

        public MyHandler(Activity activity){
            mWeakReference = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mWeakReference == null){
                return;
            }
            String message = new String((byte[])msg.obj);
            if (msg.what == MESSAGE_READ) {
                String device = ((MainActivity) mWeakReference.get()).getClientName();
                if (device != null) {
                    ((MainActivity) mWeakReference.get()).setData(device + ":" + message);
                }
            }else if (msg.what == MESSAGE_WRITE){
                ((MainActivity) mWeakReference.get()).setData("我：" + message);
            }
        }
    }

    private static final UUID APP_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");

    private class AcceptThread extends Thread{

        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread(){
            BluetoothServerSocket tmp = null;
            try{
                // 获取本地的BluetoothServerSocket
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(TAG, APP_UUID);
            }catch (IOException e){}
            mmServerSocket = tmp;
        }

        @Override
        public void run() {
            BluetoothSocket socket = null;
            while (mServerStart){
                try{
                    // 阻塞，通过BluetoothServerSocket获取BluetoothSocket
                    socket = mmServerSocket.accept();
                    // 根据BluetoothSocket获取远端设备handle
                    mDeviceName = socket.getRemoteDevice().getName();
                    if (socket != null){
                        manageConnectedSocket(socket);
                        mmServerSocket.close();
                        break;
                    }
                }catch (IOException e){
                    break;
                }
            }

        }

        public void cancel(){
            try{
                mmServerSocket.close();
            }catch (IOException e){}
        }
    }

    private class ConnectThread extends Thread{

        private final BluetoothDevice mmDevice;
        private final BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device){
            BluetoothSocket tmp = null;
            mmDevice = device;
            try{
                // 通过远端设备handle获取BluetoothSocket
                // Hint: If you are connecting to a Bluetooth serial board then try
                // using the well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB.
                // However if you are connecting to an Android peer then please generate
                // your own unique UUID.
                tmp = device.createRfcommSocketToServiceRecord(APP_UUID);
            }catch (IOException e){}
            mmSocket = tmp;
        }

        @Override
        public void run() {
            // 连接之前必须取消本地的设备可被发现
            mBluetoothAdapter.cancelDiscovery();

            try{
                // 连接
                mmSocket.connect();
            }catch (IOException e){
                try{
                    mmSocket.close();
                }catch (IOException e1){

                }
                return;
            }

            mDeviceName = mmDevice.getName();
            manageConnectedSocket4Client(mmSocket);
            // 记得close BlueToothSocket，在数据传输完成之后
        }

        public void cancel(){
            try{
                mmSocket.close();
            }catch (IOException e){}
        }
    }

    private class ConnectedThread extends Thread{

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket){

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try{
                // 通过BluetoothSocket获取流对象
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            }catch (IOException e){}

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "已连接" + mDeviceName, Toast.LENGTH_SHORT).show();
                    pbWaiting.setVisibility(View.GONE);
                }
            });
            byte[] buffer = new byte[1024];
            int bytes;

            while (true){
                try{
                    bytes = mmInStream.read(buffer);

                    mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                }catch (IOException e){
                    break;
                }
            }
        }

        public void write(byte[] bytes){
            try{
                mmOutStream.write(bytes);
                mHandler.obtainMessage(MESSAGE_WRITE, -1, -1, bytes).sendToTarget();
            }catch (IOException e){

            }finally {
                tvSend.setText("");
            }
        }

        public void cancel(){
            try{
                mmSocket.close();
            }catch (IOException e){}
        }
    }
}
