package co.lujun.bluetoothledemo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

@TargetApi(21)
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "BluetoothLEDemo";
    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 100000;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mLEScanner;
    private List<BluetoothDevice> mDeviceList;
    private List<String> mDeviceInfoList;
    private List<ScanFilter> filters;
    private BaseAdapter mDeviceAdapter;
    private Handler mHandler;
    private ScanSettings settings;
    private ListView mListView;
    private ProgressBar mProgressBar, mPbFinding;
    private Button btnFinding, btnSend;
    private EditText text4Send;
    private TextView textSR;

    private BluetoothGattCharacteristic mNotifyCharacteristic, mWriteCharacteristic;
    private BluetoothGatt mBluetoothGatt;

    private MyScanCallback mScanCallback;
    private BluetoothDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initBlueToothLE();
    }

    private void init(){
        mListView = (ListView) findViewById(R.id.listview);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        btnFinding = (Button) findViewById(R.id.btn_finding);
        mPbFinding = (ProgressBar) findViewById(R.id.pb_finding);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            Toast.makeText(this, "不支持BLE", Toast.LENGTH_SHORT).show();
            finish();
        }
        mHandler = new Handler();
        mDeviceList = new ArrayList<BluetoothDevice>();
        mDeviceInfoList = new ArrayList<String>();
        mDeviceAdapter = new ArrayAdapter<String>(this, R.layout.device_item, mDeviceInfoList);
        mListView.setAdapter(mDeviceAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String info = mDeviceInfoList.get(position);
                String address = info.substring(info.length() - 17);
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                connectToDevice(device);
            }
        });
        btnFinding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startScan();
            }
        });

        text4Send = (EditText) findViewById(R.id.text_send);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(text4Send.getText().toString());
            }
        });

        textSR = (TextView) findViewById(R.id.text_sr);
    }

    private void initBlueToothLE(){
        mDeviceList.clear();
        mDeviceInfoList.clear();
        // get BluetoothAdapter
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null){
            Toast.makeText(this, "不支持蓝牙", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }

    private void startScan(){
        mPbFinding.setVisibility(View.VISIBLE);
        if (!mBluetoothAdapter.isEnabled()){
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        }else {
            if (Build.VERSION.SDK_INT >= 21){
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<ScanFilter>();
            }
            scanLeDevice(false);
            scanLeDevice(true);
        }
    }

    // 传统蓝牙设备或者BLE设备，两者完全独立，不可同时被搜索
    private BluetoothAdapter.LeScanCallback mLeScanCallback
            = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            //TODO  run on ui thread
            final BluetoothDevice device1 = device;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDeviceList.add(device1);
                    mDeviceInfoList.add(device1.getName() + "@" + device1.getAddress());
                    mDeviceAdapter.notifyDataSetChanged();
                    Log.d(TAG, "LeScanCallback-onLeScan,info:" + device1.getName() + "@" + device1.getAddress());
                }
            });

        }
    };

    private class MyScanCallback extends ScanCallback{
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            final BluetoothDevice device = result.getDevice();
            final int callbackType1 = callbackType;
            final ScanResult result1 = result;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDeviceList.add(device);
                    mDeviceInfoList.add(device.getName() + "@" + device.getAddress());
                    mDeviceAdapter.notifyDataSetChanged();
                    Log.d(TAG, "ScanCallback-onScanResult,callbackType:" + callbackType1);
                    Log.d(TAG, "ScanCallback-onScanResult,result:" + result1);
                }
            });

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            for (ScanResult result : results){
                Log.d(TAG, "ScanCallback-onBatchScanResults, result:" + result.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d(TAG, "ScanCallback-onScanFailed, errorcode:" + errorCode);
        }
    }

    private BluetoothGattCallback mGattCallBack = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d(TAG, "onConnectionStateChange,status:" + status);
            switch (newState){
                case BluetoothProfile.STATE_CONNECTED:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "已连接：" + mDevice.getName() + "@" + mDevice.getAddress(),
                                    Toast.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                        }
                    });
                    Log.d(TAG, "onConnectionStateChange,state:STATE_CONNECTED");
                    gatt.discoverServices();// 搜索连接设备所支持的service
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.d(TAG, "onConnectionStateChange,state:STATE_DISCONNECTED");
                    break;
                default:
                    Log.d(TAG, "onConnectionStateChange,state:STATE_OTHER");
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                List<BluetoothGattService> services = gatt.getServices();
                for (BluetoothGattService service : services) {
                    Log.d(TAG, "onServicesDiscovered,service-uuid:" + service.getUuid().toString());
                    List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                    for (BluetoothGattCharacteristic characteristic : characteristics) {
                        Log.d(TAG, "onServicesDiscovered,characteristic-uuid:" + characteristic.getUuid().toString());
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PERMISSION_READ) > 0){
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null){
                                mBluetoothGatt.setCharacteristicNotification(mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            gatt.readCharacteristic(characteristic);// 读取指定的characteristic
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0){
                            mNotifyCharacteristic = characteristic;
                            mBluetoothGatt.setCharacteristicNotification(characteristic, true);
                        }
                        if (((charaProp & BluetoothGattCharacteristic.PERMISSION_WRITE)
                                | (charaProp & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) > 0){
                                mWriteCharacteristic = characteristic;
                        }

                    }
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
//            Log.d(TAG, "onCharacteristicRead,characteristic:" + characteristic);
            Log.d(TAG, "onCharacteristicRead,data:" + parseData(characteristic));
//            gatt.disconnect();// 断开与远程设备的GATT连接
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            final BluetoothGattCharacteristic characteristic1 = characteristic;
            Log.d(TAG, "onCharacteristicWrite,");runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textSR.append("S: " + parseData(characteristic1) + "\n");
                }
            });

        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d(TAG, "onCharacteristicChanged,data:" + parseData(characteristic));
            Log.d("debugss", characteristic.getUuid().toString());
            final BluetoothGattCharacteristic characteristic1 = characteristic;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textSR.append("R: " + parseData(characteristic1) + "\n");
                }
            });

        }
    };

    private String parseData(BluetoothGattCharacteristic characteristic){
        String result = "";
        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
//        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
//            int flag = characteristic.getProperties();
//            int format = -1;
//            if ((flag & 0x01) != 0) {
//                format = BluetoothGattCharacteristic.FORMAT_UINT16;
//                Log.d(TAG, "Heart rate format UINT16.");
//            } else {
//                format = BluetoothGattCharacteristic.FORMAT_UINT8;
//                Log.d(TAG, "Heart rate format UINT8.");
//            }
//            final int heartRate = characteristic.getIntValue(format, 1);
//            Log.d(TAG, String.format("Received heart rate: %d", heartRate));
//            intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
//        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for(byte byteChar : data)
                // 格式： 转化为16进制，最小两位一组，不足两位前面补0，大于等于两位不管
                    stringBuilder.append(String.format("%02X", byteChar));
//                intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
                // 原内容+ 16进制内容
//                result =  new String(data) + "\n" + stringBuilder.toString();
                result =  new String(data);
            }
//        }
        return result;
    }

    private void sendData(String data){
        if (!TextUtils.isEmpty(data) && mWriteCharacteristic != null){
//            BluetoothGattDescriptor descriptor = mWriteCharacteristic.getDescriptor(null);
//            descriptor.setValue(strBytes)

            byte[] strBytes = data.getBytes();
            byte[] bytes = mWriteCharacteristic.getValue();
            mWriteCharacteristic.setValue(strBytes);
            mBluetoothGatt.writeCharacteristic(mWriteCharacteristic);
        }
    }


    private void connectToDevice(BluetoothDevice device){
        mProgressBar.setVisibility(View.VISIBLE);
        if (mBluetoothGatt!= null && mBluetoothGatt.connect()){
            return;
        }
        if (mBluetoothGatt == null){
//            BluetoothGatt常规用到的几个操作示例:
//            connect() ：连接远程设备。
//            discoverServices() : 搜索连接设备所支持的service。
//            disconnect()：断开与远程设备的GATT连接。
//            close()：关闭GATTClient端。
            mDevice = device;
            mBluetoothGatt = device.connectGatt(this, false, mGattCallBack);
            scanLeDevice(false);
        }
    }

    private void scanLeDevice(final boolean enable){
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mPbFinding.setVisibility(View.GONE);
                        }
                    });
                    stopScan();
                }
            }, SCAN_PERIOD);
            if (Build.VERSION.SDK_INT < 21) {
                // 搜索指定UUID的外设, startLeScan(UUID[], BluetoothAdapter.LeScanCallback)
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }else {
                if (mScanCallback == null) mScanCallback = new MyScanCallback();
                    mLEScanner.startScan(filters, settings, mScanCallback);
            }
        }else{
            stopScan();
        }
    }

    private void stopScan(){
        if (Build.VERSION.SDK_INT < 21) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }else {
            mLEScanner.stopScan(mScanCallback);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT){
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "蓝牙已开启");
                scanLeDevice(true);
            }else if (resultCode == Activity.RESULT_CANCELED){
                Log.d(TAG, "蓝牙未开启");
                finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()){
            scanLeDevice(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBluetoothGatt == null) return;
        mBluetoothGatt.close();// 关闭GATTClient端
        mBluetoothGatt = null;
    }
}
